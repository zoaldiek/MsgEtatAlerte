from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'ACfa126b0ee9f8e6c6b6c1b78fbb0ea584'
auth_token = '5bcecd894acdcde126ab2670947b6db7'
client = Client(account_sid, auth_token)

message = client.messages \
    .create(
         body='This is the ship that made the Kessel Run in fourteen parsecs?',
         from_='+18135483880',
         to='+33603350408'
     )

print(message.sid)
